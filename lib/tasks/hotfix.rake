namespace :fix do
  desc "Genereate alpha version for cover_photos"
  task :g_alpha => :environment do
    IncoFinding.all.each do |finding|
      finding.cover_photo.recreate_versions!
    end
  end

  task :fill_content_type => :environment do
    IncoPicture.all.each do |pic|
      pic.file_content_type = pic.photo.content_type
      pic.save!
    end
  end

  task :fill_custom_url => :environment do
    IncoProject.all.each do |project|
      project.custom_url = project.id
      project.showing = true
      project.save!
    end
  end
end
