class DashboardController < ApplicationController
  http_basic_authenticate_with name: Settings.dashboard.username, password: Settings.dashboard.password
end
