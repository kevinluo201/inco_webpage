class IncoFindingsController < ApplicationController
  before_action :set_inco_finding, only: [:show, :edit, :update, :destroy]

  # GET /inco_findings
  # GET /inco_findings.json
  def index
    @inco_findings = IncoFinding.all
  end

  # GET /inco_findings/1
  # GET /inco_findings/1.json
  def show
  end

  # GET /inco_findings/new
  def new
    @inco_finding = IncoFinding.new
  end

  # GET /inco_findings/1/edit
  def edit
  end

  # POST /inco_findings
  # POST /inco_findings.json
  def create
    @inco_finding = IncoFinding.new(inco_finding_params)

    respond_to do |format|
      if @inco_finding.save
        format.html { redirect_to @inco_finding, notice: 'Inco finding was successfully created.' }
        format.json { render :show, status: :created, location: @inco_finding }
      else
        format.html { render :new }
        format.json { render json: @inco_finding.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /inco_findings/1
  # PATCH/PUT /inco_findings/1.json
  def update
    respond_to do |format|
      if @inco_finding.update(inco_finding_params)
        format.html { redirect_to @inco_finding, notice: 'Inco finding was successfully updated.' }
        format.json { render :show, status: :ok, location: @inco_finding }
      else
        format.html { render :edit }
        format.json { render json: @inco_finding.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /inco_findings/1
  # DELETE /inco_findings/1.json
  def destroy
    @inco_finding.destroy
    respond_to do |format|
      format.html { redirect_to inco_findings_url, notice: 'Inco finding was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_inco_finding
      @inco_finding = IncoFinding.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def inco_finding_params
      params.require(:inco_finding).permit(:cover_photo, :description_zh, :description_en)
    end
end
