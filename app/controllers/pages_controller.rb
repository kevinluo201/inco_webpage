class PagesController < ApplicationController
  def index
    @projects = IncoProject.where(showing: true).order(:sort)
  end

  def whywhatwho
  end

  def projects
    @projects = IncoProject.where(showing: true).order(:sort)
  end

  def project
    @project = IncoProject.find_by_custom_url(params[:custom_url])
    @process = @project.inco_process
    @final = @project.inco_final
    @finding = @project.inco_finding

  rescue Excon::Error::Socket
    redirect_to project_path(params[:custom_url])
  end

  def contact
  end

  def contact_email
    # Sends email to hi@in-co.studio with comment
    InCoMailer.contact_email(params[:email], params[:content]).deliver

    redirect_to root_path
  end

  def subscribe_email
    # Sends email to user hi@in-co.studio for subscription
    InCoMailer.subscribe_email(params[:email_subs]).deliver

    redirect_to root_path
  end

  def swap_locale
    I18n.locale = (params[:lang] == 'en') ? :en : :zh
    session[:locale] = I18n.locale
    redirect_to :back
  end
end
