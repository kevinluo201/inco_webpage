class IncoProjectsController < DashboardController
  before_action :set_inco_project, only: [:show, :edit, :update, :destroy]

  # GET /inco_projects
  def index
    @inco_projects = IncoProject.order(:sort).all
  end

  # GET /inco_projects/1
  def show
  end

  # GET /inco_projects/new
  def new
    @inco_project = IncoProject.new
  end

  # GET /inco_projects/1/edit
  def edit
  end

  # POST /inco_projects
  def create
    @inco_project = IncoProject.new(inco_project_params)
    if @inco_project.save
      redirect_to @inco_project, notice: 'Inco project was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /inco_projects/1
  def update
    if @inco_project.update(inco_project_params)
      redirect_to @inco_project, notice: 'Inco project was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /inco_projects/1
  def destroy
    @inco_project.destroy
    redirect_to inco_projects_url, notice: 'Inco project was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_inco_project
    @inco_project = IncoProject.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def inco_project_params
    params.require(:inco_project).permit(:title_zh, :title_en, :description_zh,
                                         :description_en, :challenge_zh, :challenge_en,
                                         :location, :status, :partners, :dates,
                                         :services_zh, :services_en, :keywords_zh,
                                         :keywords_en, :process_title_zh,
                                         :process_title_en, :cover_photo, :inco_category,
                                         :showing, :custom_url, :site_url)
  end
end
