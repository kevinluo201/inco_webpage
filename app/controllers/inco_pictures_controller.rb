class IncoPicturesController < DashboardController
  before_action :set_inco_picture, only: [:show, :edit, :update, :destroy]
  before_action :set_inco_content, only: [:index, :new, :create]

  # GET /inco_pictures
  # GET /inco_pictures.json
  def index
    @inco_pictures = @inco_content.inco_pictures.order(:sort)
  end

  # GET /inco_pictures/1
  # GET /inco_pictures/1.json
  def show
  end

  # GET /inco_pictures/new
  def new
    @inco_picture = IncoPicture.new(inco_content: @inco_content)
  end

  # GET /inco_pictures/1/edit
  def edit
  end

  # POST /inco_pictures
  # POST /inco_pictures.json
  def create
    @inco_picture = IncoPicture.new(inco_picture_params.merge(inco_content: @inco_content))

    respond_to do |format|
      if @inco_picture.save
        format.html { redirect_to @inco_picture, notice: 'Inco picture was successfully created.' }
        format.json { render :show, status: :created, location: @inco_picture }
      else
        format.html { render :new }
        format.json { render json: @inco_picture.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /inco_pictures/1
  # PATCH/PUT /inco_pictures/1.json
  def update
    respond_to do |format|
      if @inco_picture.update(inco_picture_params)
        format.html { redirect_to @inco_picture, notice: 'Inco picture was successfully updated.' }
        format.json { render :show, status: :ok, location: @inco_picture }
      else
        format.html { render :edit }
        format.json { render json: @inco_picture.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /inco_pictures/1
  # DELETE /inco_pictures/1.json
  def destroy
    content = @inco_picture.inco_content
    back_url = content.is_a?(IncoProcess) ? inco_process_inco_pictures_url(content) :
                                            inco_final_inco_pictures_url(content)
    @inco_picture.destroy
    respond_to do |format|
      format.html { redirect_to back_url, notice: 'Inco picture was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_inco_picture
      @inco_picture = IncoPicture.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def inco_picture_params
      params.require(:inco_picture).permit(:photo, :is_photo, :youtube_id, :title_color, :poster)
    end

    def set_inco_content
      @inco_content = if params[:inco_process_id].present?
                        IncoProcess.find(params[:inco_process_id])
                      else
                        IncoFinal.find(params[:inco_final_id])
                      end
    end
end
