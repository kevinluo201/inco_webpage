//NAV
function nav() {
  $("#menu").click(function() {
    pt = ($('#section6').height() - menu_height()) / 2;
    $('nav').css('padding-top', pt);
    $("nav").fadeIn();
  });
  $(".menuon").click(function() {
    $("nav").fadeOut();
  });
}

function album() {
  $(".small img").click(function() {
    var NN = $(this).index();
    $(".banner img").eq(NN).fadeIn(800).siblings().fadeOut(800);
  });
}

function carousel() {
  $(".carousel").carousel();
  $(".carousel02").carousel();
}

function contact_section(){
  // 避免點不到google map，所以限制contact section的寬度
  adjust_contact_info();
  $(window).resize(function(){
    adjust_contact_info();
  });
}

function adjust_contact_info(){
  if($('#section6 .nav_left').offset() != undefined){
    var m_left = $('#section6 .nav_left').offset().left;
    $('.container_info').css('margin-left', m_left);
    if(!isMobile())
      $('.container_info').css('width', 'calc(60% - ' + m_left + 'px)');
  }

  $('.container_info').css('margin-top', ($(window).height() - $('.container_info').height()) / 2);
}

function homepage_projects() {
  // initiate the project category h3
  $('#project_category a').html($('.project').first().attr('category'));
  q =
    $('#project_category a').attr("href", function(ind, attr) {
      return attr + '?q=' + $('.project').first().attr('category');
    });

  $('#inco_projects').slick({
    arrows: true,
    responsive: [{

      breakpoint: 768,
      settings: {
        arrows: false
      }

    }]
  });

  // On before slide change
  $('#inco_projects').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
    $('#project_category a').attr("href", function(ind, attr) {
      q = $(".slick-current").next().attr('category');
      // if the href contains "?", append &x=z, else, append ?x=z
      return attr.split('?')[0] + '?q=' + q;
    });

    $('#project_category a').fadeOut(function() {
      $(this).text($(".slick-current").attr('category'));
    }).fadeIn();

  });
}

function projects_page() {
  category = getUrlVars()["q"];
  projects_present(category);
  if(category != undefined)
    $('.category_btn[category=' + category + ']').addClass('active');
  else
    $('.category_btn[category="all"]').addClass('active');

  $('.category_btn').click(function(){
    $('.category_btn').removeClass('active');
    $(this).addClass('active');
    projects_present($(this).attr('category'));
  });

  mt = ($('#section6').height() - menu_height()) / 2;
  $('#section6 .container').css('padding-top', mt);

  // position the copyright
  $('#section6').css('position', 'relative');

  // for smoothly scroll down to footer
  // $(window).scroll(function(){
  //   scrollBottom = $(window).scrollTop() + $(window).height();
  //   footer_top = $('body.projects_body #section6').position()['top'];
  //   if(scrollBottom >= footer_top){
  //     $("body, html").animate({
  //         scrollTop: $("body.projects_body #section6").position().top
  //     });
  //   }
  // });
}

function projects_present(category) {
  $('.project').fadeOut();
  if(category == 'all' || category == undefined){
    $('.project').fadeIn();
  }
  else{
    $('.project[category=' + category + ']').fadeIn();
  }
}

function menu_height(){
  menus = $('#section6 .container');
  return Math.max(menus.first().height(), menus.last().height());
}

function project_page() {
  $('.heros>img').css({
    position: 'absolute',
    left: ($('.heros').width() - $('.heros>img').width()) / 2
  });

  $('.final_content').click(function(){
    $('.final_content').slick('slickNext');
  });
  $('.video_for').click(function(){
    $('.video_for').slick('slickNext');
  });

  $('.final_content').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
    $('#final_title').removeClass('light dark');
    $('#final_title').addClass($(".slick-current").attr('title_color'));
  });

  if($('.project_title02').offset() != undefined )
    $('#final_title').css('left', $('.project_title02').offset()['left']);

  $('.video_for').on('init', function(){
    $('.video_for img').css('height', $('.video_for img').width() / 16 * 9);
  });


  $('#video_nav').on('init', function(){
    $('#video_nav .slick-slide').css({
      width: $('#video_nav').width() / 4,
      height: $('#video_nav').width() / 4 / 16 * 9
    });
  });
}

// Read a page's GET URL variables and return them as an associative array.
function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function isMobile(){
  if( $(window).width() < 768 )
    return true;
  else
    return false;
}

$(document).ready(function() {
  nav();
  album();
  carousel();
  homepage_projects();
  projects_page();
  project_page();
  contact_section();
});
