class InCoMailer < ApplicationMailer
  default to: "hi@in-co.studio"

  def contact_email(email, content)
    @content = content
    mail(from: email, subject: 'Contact Email')
  end

  def subscribe_email(email)
    @email = email
    mail(from: email, subject: 'Subscription Email')
  end
end
