class IncoPicture < ApplicationRecord
  mount_uploader :photo, PhotoUploader
  mount_uploader :poster, PosterUploader
  belongs_to :inco_content, polymorphic: true

  include RailsSortable::Model
  set_sortable :sort  # Indicate a sort column

  def is_image?
    return false if self.file_content_type.nil?
    self.file_content_type.start_with? 'image'
  end
end
