class IncoProject < ApplicationRecord
  include IncoTranslate
  include RailsSortable::Model
  set_sortable :sort  # Indicate a sort column

  after_create :build_contents
  mount_uploader :cover_photo, CoverPhotoUploader
  # 由於custom_url取代了finding的id，所以是必須的值
  validates :custom_url, presence: true

  has_one :inco_process, dependent: :destroy
  has_one :inco_final, dependent: :destroy
  has_one :inco_finding, dependent: :destroy

  private

  def build_contents
    self.inco_process = IncoProcess.create!
    self.inco_final = IncoFinal.create!
    self.inco_finding = IncoFinding.create!
  end
end
