class IncoFinding < ApplicationRecord
  include IncoTranslate
  mount_uploader :cover_photo, CoverPhotoUploader
  belongs_to :inco_project
end
