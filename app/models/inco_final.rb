class IncoFinal < ApplicationRecord
  belongs_to :inco_project
  has_many :inco_pictures, as: :inco_content, dependent: :destroy
end
