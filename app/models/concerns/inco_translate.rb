module IncoTranslate
  # create translation
  def method_missing(method_name, *args, &block)
    # check if method is an attribute, if yes then call the method
    attribute = method_name.to_s + "_#{I18n.locale}"
    if self.respond_to?(attribute)
      self.send attribute
    else
      super
    end
  end
end
