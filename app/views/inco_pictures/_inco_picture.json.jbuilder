json.extract! inco_picture, :id, :photo, :photo_or_video, :youtube_id, :created_at, :updated_at
json.url inco_picture_url(inco_picture, format: :json)
