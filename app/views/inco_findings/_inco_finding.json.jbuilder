json.extract! inco_finding, :id, :cover_photo, :description_zh, :description_en, :created_at, :updated_at
json.url inco_finding_url(inco_finding, format: :json)
