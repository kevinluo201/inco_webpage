module IncoPicturesHelper
  def pictures_index_path
    if @inco_picture.inco_content
      if @inco_picture.inco_content_type == 'IncoProcess'
        inco_process_inco_pictures_path(@inco_picture.inco_content)
      else
        inco_final_inco_pictures_path(@inco_picture.inco_content)
      end
    elsif params[:inco_process_id].present?
      inco_process_inco_pictures_path
    else
      inco_final_inco_pictures_path
    end
  end

  def new_picture_path
    if params[:inco_process_id].present?
      new_inco_process_inco_picture_path
    else
      new_inco_final_inco_picture_path
    end
  end

  def youtube_thumbnail(inco_picture)
    "http://img.youtube.com/vi/#{inco_picture.youtube_id}/0.jpg"
  end
end
