module PagesHelper
  def team_members
    [
      {
        name: "Jessica Lee",
        short: "jessica",
        title: "Founder",
        photo: "team/J_before.png",
        photo_hover: "team/J_after.png",
        modal: ".jessica-modal-lg"
      },
      {
        name: "Modi Hung",
        short: "modi",
        title: "Research Designer",
        photo: "team/Modi_before.png",
        photo_hover: "team/Modi_after.png",
        modal: ".modi-modal-lg"
      },
      {
        name: "Nobi Yo",
        short: "nobi",
        title: "Research Designer",
        photo: "team/Nobi_before.png",
        photo_hover: "team/Nobi_after.png",
        modal: ".nobi-modal-lg"
      },
      {
        name: "David Liu",
        short: "david",
        title: "Research Designer",
        photo: "team/David_before.png",
        photo_hover: "team/David_after.png",
        modal: ".nobi-modal-lg"
      },
    ]
  end
end
