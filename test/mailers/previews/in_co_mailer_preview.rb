# Preview all emails at http://localhost:3000/rails/mailers/in_co_mailer
class InCoMailerPreview < ActionMailer::Preview
  def contact_email_preview
    InCoMailer.contact_email('test@test.com', 'test message')
  end
end
