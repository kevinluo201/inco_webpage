Rails.application.routes.draw do
  # pages
  root 'pages#index'
  get 'whywhatwho', to: 'pages#whywhatwho', as: 'whywhatwho'
  get 'contact', to: 'pages#contact', as: 'contact'
  post 'contact_email', to: 'pages#contact_email', as: 'contact_email'
  post 'subscribe_email', to: 'pages#subscribe_email', as: 'subscribe_email'
  get 'projects', to: 'pages#projects', as: 'projects'
  get 'projects/:custom_url', to: 'pages#project', as: 'project'
  get 'swap_locale', to: 'pages#swap_locale', as: 'swap_locale'

  resources :inco_projects
  resources :inco_findings, only: [:show, :edit, :update]
  resources :inco_processes, :inco_finals, only: [] do
    resources :inco_pictures, shallow: true
  end
end
