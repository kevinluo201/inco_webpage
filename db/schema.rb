# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190609012033) do

  create_table "inco_finals", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "inco_project_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["inco_project_id"], name: "index_inco_finals_on_inco_project_id", using: :btree
  end

  create_table "inco_findings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "cover_photo"
    t.text     "description_zh",  limit: 65535
    t.text     "description_en",  limit: 65535
    t.integer  "inco_project_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "inco_pictures", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "photo"
    t.boolean  "is_photo"
    t.string   "youtube_id"
    t.integer  "inco_content_id"
    t.string   "inco_content_type"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "file_content_type"
    t.integer  "sort"
    t.string   "title_color"
    t.string   "poster"
    t.index ["inco_content_type", "inco_content_id"], name: "index_inco_pictures_on_inco_content_type_and_inco_content_id", using: :btree
  end

  create_table "inco_processes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "inco_project_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["inco_project_id"], name: "index_inco_processes_on_inco_project_id", using: :btree
  end

  create_table "inco_projects", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "title_zh"
    t.string   "title_en"
    t.text     "description_zh",   limit: 65535
    t.text     "description_en",   limit: 65535
    t.text     "challenge_zh",     limit: 65535
    t.text     "challenge_en",     limit: 65535
    t.string   "location"
    t.string   "status"
    t.string   "partners"
    t.string   "dates"
    t.string   "keywords_zh"
    t.string   "keywords_en"
    t.string   "process_title_zh"
    t.string   "process_title_en"
    t.string   "cover_photo"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "services_en"
    t.string   "services_zh"
    t.string   "inco_category"
    t.integer  "sort"
    t.boolean  "showing"
    t.string   "custom_url"
    t.string   "site_url"
    t.index ["custom_url"], name: "index_inco_projects_on_custom_url", using: :btree
  end

  add_foreign_key "inco_finals", "inco_projects"
  add_foreign_key "inco_processes", "inco_projects"
end
