# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


project = IncoProject.new(title_zh: '思考政府市民溝通 - 嘉義',
                          title_en: 'Bridging Communication in Chiayi',
                          cover_photo: Rails.root.join("app/assets/images/hpm.jpg").open,
                          location: 'Chiayi, Taiwan 23° 28′ 48.27″ N, 120° 26′ 56.8″ E',
                          dates: '2017.09 – 2017.11',
                          status: 'Completed',
                          services_zh: '影片',
                          services_en: 'Video',
                          partners: '李俊俋委員、陳治維（顧問）、馮子聰（攝影剪輯）、參與訪談的嘉義市民',
                          keywords_zh: '嘉義、鐵路、鐵路高架化、平交道、陸橋、地下道、交通、東區、西區、都市更新',
                          keywords_en: 'Chiayi、railway、railway track elevation、level crossing、bridge、underpass、traspotation、partition、urban renewal',
                          process_title_zh: '側拍嘉義市現況',
                          process_title_en: "Chiayi City's current status" ,
                          custom_url: 'custom_url'
                          )

project.description_zh = <<-END.gsub(/^\s+\|/, '')
  |嘉義市立法委員李俊俋委託我們製作即將開始動工為期 9 年的嘉義市鐵路高架化市民溝通影片，這支影片的目的是傳達工程重要資訊，將複雜的政策進行「轉譯」，以視覺化的方式簡明扼要地讓不同年齡層的觀看者清楚知道最新的工程狀況。
  |
  |而這項任務也讓 in-co 團隊重新思考政府和人民之間最適合的溝通模式，顯然地，影片相較於文字，能夠更直白的將資訊傳遞，此外，我們還需要去探討影片的呈現手法、設計、編排方式等等，我們希望透過此項任務，不僅能成功讓嘉義市民了解鐵路高架化，我們也能夠從中得到溝通方面的經驗。
END

project.description_en = <<-END.gsub(/^\s+\|/, '')
  |Chiayi City Legislator Lee Chun-yi contracted in-co to create a communications video for the 9-year-long Chiayi Railway Elevation Project. This video seeks to convey key project information and to “translate” complex policies into concise and visually understandable format for viewers of all ages and of various backgrounds.
  |
  |This task allows the in-co team to rethink the most appropriate mode of communication between the government and the people in which it represents. In exploring this topic, it was determined that a film is more straightforward to deliver than a descriptive text. In addition, it is the hope that this task will not only successfully convey to the people of Chiayi the elevated railway proposal, but that it will unlock the potential for further experiences and explorations in communication.
END


project.challenge_zh = "以清楚、簡單、好理解為目標，思考傳達資訊的方式，在資訊變化快速的當代，仍然可以有效建立政府與人民間溝通的橋樑。"
project.challenge_en = "In the midst of rapid exchanging of information and changing of technology, the problem of miscommunication (or no communication) between the public and the government still persists. the challenge is to create a medium that can effectively bridge the two parties and allow transparent and effective communications."

project.save!

project.inco_finding.cover_photo = Rails.root.join("app/assets/images/finding.jpg").open

project.inco_finding.description_zh = <<-END.gsub(/^\s+\|/, '')
  |一開始委員是希望我們能製作與其他交通建設工程一樣的 3D 模型影片，但我們認為這樣的影片並不能讓嘉義市民有感受或是對於高架化有認同感，因此我們決定親身去了解市民對於高架化的想法，過程中我們了解到溝通的重要性。<br>
  |<br>
  |我們走訪嘉義，詢問市民是否知道這項工程，令我們意外的是，即便嘉義車站外的圍牆看板上已經公告了高架化的宣傳廣告，在我們訪問的市民中，還有許多人並不知道鐵路即將要高架化，對於這方面的資訊相當缺乏。<br>
  |<br>
  |許多嘉義市民對於高架化的進度並不熟悉所以感到困惑與擔心，擔心這項巨大的變化會不會帶來許多負面的影響，當然還有巨大的金額會不會讓嘉義市負債更嚴重，但這方面的訊息，好像失去了適當的管道來溝通。其實，這種種的問題不管是政府還是委員都為了能夠實現高架化做了許多努力，例如已經將嘉義市政府原本近 100 億元的支出縮減至 38.95 億元，像這些訊息都應該即時告知市民，讓市民可在第一時間追蹤工程的走向。<br>
  |<br>
  |我們試圖用我們的影片來傳達並有效的溝通，我們將影片分成三部分，第一部分簡單的解釋目前嘉義市交通的問題以及高架化之後對於平面交通帶來的改變；第二部分讓市民了解到高架化後的橋下空間是可以充滿想像的，可以為嘉義市帶來新的改變；第三部分則將解釋整個工程上的範圍、時間、金額等等細節。<br>
  |<br>
  |我們想，深入與當地民眾溝通，才能夠真正有效的知道民眾想要知道的問題所在，才能夠對症下藥的達到雙方資訊對等的結果。在未來，我們期許我們任何的項目都能夠達到有效的溝通，建立一個能夠達持共識的橋梁。<br>
END

project.inco_finding.description_en = <<-END.gsub(/^\s+\|/, '')
  |At the beginning, Legislator Lee hoped that in-co would make the same 3D model films as other construction projects. However, it was determined that such films will not resonate with the public or the public simply cannot relate to these films personally. We believe that we need to communicate with the public directly in order to find out what the public perceives of the project and their visions of Chiayi. During the process of conducting field works and interviews with locals, not only we reaffirm the importance communication in understanding the voices of people, we also realized how distant the government is to its people.<br>
  |<br>
  |In Chiayi, citizens were asked if they knew anything about the upcoming project. Surprisingly, even though billboards outside of Chiayi Station had been advertising the elevated proposal, many of the citizens interviewed knew nothing of the railway being lifted. This lack of information exemplifies the disconnect between the government and the citizenry.<br>
  |<br>
  |Since many people in Chiayi are not familiar with the progress of the project, they are worried that such a large change could affect them negatively. A project at such a scale does indeed incur a lot of debt for a city such as Chiayi. Lack of communication on public projects of such scales creates a sense of public insecurity which leads to lack of public support. In fact, much efforts have been made by Legistor Lee and the city government of Chiayi in reducing a 10 billion yuan expenditure to 3.895 billion yuan.Information as such should immediately inform the people to make them understand the government is trying to manage the potential impact induced by public project of such scale.<br>
  |<br>
  |Legislator Lee understood such disconnect between the government and the people, hence the commision of the creation of this communication video for the people in Chiayi. The video is divided into three parts. The first briefly explains the current state of traffic (overpass, underpass and on-grade pass) in Chiayi City and the changes that would be brought about by elevating the rail. The second raises awareness of what could happen below the elevated tracks and how those possibilities could enhance public life in Chiayi City. The third part of the video explains project scope, schedule and other details pertaining to project execution.<br>
  |<br>
  |By thoroughly communicating with the local people, finding out what information the public would like to possess, the right balance of information between the two parties can be reached. In the future, any projects forwarded by in-co will have expectations of effective communication which will be used to build a bridge of consensus.<br>
END

project.inco_finding.save!

#Process
project.inco_process.inco_pictures.
        create([{ is_photo: true, photo: Rails.root.join("app/assets/images/process_img.jpg").open },
                { youtube_id: 'V1bFr2SWP1I' },
                { youtube_id: 'd27gTrPPAyk'}])

#Final
project.inco_final.inco_pictures.create(youtube_id: 'aLnZ1NQm2uk')








