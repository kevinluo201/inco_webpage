class CreateIncoFindings < ActiveRecord::Migration[5.0]
  def change
    create_table :inco_findings do |t|
      t.string :cover_photo
      t.text :description_zh
      t.text :description_en
      t.integer :inco_project_id

      t.timestamps
    end
  end
end
