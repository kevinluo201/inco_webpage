class AddHidingAndCustomUrlToIncoProjects < ActiveRecord::Migration[5.0]
  def change
    add_column :inco_projects, :showing, :boolean
    add_column :inco_projects, :custom_url, :string

    add_index :inco_projects, :custom_url
  end
end
