class AddPosterToIncoPicture < ActiveRecord::Migration[5.0]
  def change
    add_column :inco_pictures, :poster, :string
  end
end
