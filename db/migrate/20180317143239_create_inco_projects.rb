class CreateIncoProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :inco_projects do |t|
      t.string :title_zh
      t.string :title_en
      t.text :description_zh
      t.text :description_en
      t.text :challenge_zh
      t.text :challenge_en
      t.string :location
      t.string :status
      t.string :partners
      t.string :dates
      t.string :services
      t.string :keywords_zh
      t.string :keywords_en
      t.string :process_title_zh
      t.string :process_title_en
      t.string :cover_photo

      t.timestamps
    end
  end
end
