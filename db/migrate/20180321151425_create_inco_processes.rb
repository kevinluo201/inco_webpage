class CreateIncoProcesses < ActiveRecord::Migration[5.0]
  def change
    create_table :inco_processes do |t|
      t.references :inco_project, foreign_key: true

      t.timestamps
    end
  end
end
