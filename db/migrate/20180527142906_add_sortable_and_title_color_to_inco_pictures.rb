class AddSortableAndTitleColorToIncoPictures < ActiveRecord::Migration[5.0]
  def change
    add_column :inco_pictures, :sort, :integer
    add_column :inco_pictures, :title_color, :string
  end
end
