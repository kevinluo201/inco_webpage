class AddIncoCategoryToIncoProject < ActiveRecord::Migration[5.0]
  def change
    add_column :inco_projects, :inco_category, :string
  end
end
