class AddFileContentTypeToIncoPictures < ActiveRecord::Migration[5.0]
  def change
    add_column :inco_pictures, :file_content_type, :string
  end
end
