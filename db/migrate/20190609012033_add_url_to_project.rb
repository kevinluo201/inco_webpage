class AddUrlToProject < ActiveRecord::Migration[5.0]
  def change
    add_column :inco_projects, :site_url, :string
  end
end
