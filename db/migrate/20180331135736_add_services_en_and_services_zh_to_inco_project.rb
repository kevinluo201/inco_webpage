class AddServicesEnAndServicesZhToIncoProject < ActiveRecord::Migration[5.0]
  def change
    remove_column :inco_projects, :services
    add_column :inco_projects, :services_en, :string
    add_column :inco_projects, :services_zh, :string
  end
end
