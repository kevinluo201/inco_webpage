class CreateIncoPictures < ActiveRecord::Migration[5.0]
  def change
    create_table :inco_pictures do |t|
      t.string :photo
      t.boolean :is_photo
      t.string :youtube_id
      t.integer :inco_content_id
      t.string :inco_content_type

      t.timestamps
    end

    add_index :inco_pictures, [:inco_content_type, :inco_content_id]
  end
end
