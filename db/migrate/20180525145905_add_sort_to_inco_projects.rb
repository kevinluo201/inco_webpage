class AddSortToIncoProjects < ActiveRecord::Migration[5.0]
  def change
    add_column :inco_projects, :sort, :integer
  end
end
